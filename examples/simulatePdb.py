from openmm.app import *
from openmm import *
from openmm.unit import *
from sys import stdout

pdb = PDBFile('input.pdb')
forcefield = ForceField('amber14-all.xml', 'amber14/tip3pfb.xml')
system = forcefield.createSystem(pdb.topology, nonbondedMethod=PME, nonbondedCutoff=1*nanometer, constraints=HBonds)
integrator = LangevinMiddleIntegrator(300*kelvin, 1/picosecond, 0.004*picoseconds)
platformProperties = {'Precision': 'double'}
platformProperties["DeviceIndex"] = "0,3"
platform = Platform.getPlatformByName('OpenCL')
simulation = Simulation(pdb.topology, system, integrator, platform, platformProperties)
#platform = Platform.getPlatformByName('CPU') 
#simulation = Simulation(pdb.topology, system, integrator, platform)
simulation.context.setPositions(pdb.positions)
simulation.minimizeEnergy()
simulation.reporters.append(PDBReporter('output.pdb', 1000))
simulation.reporters.append(StateDataReporter(stdout, 1000, step=True, speed=True, potentialEnergy=True, temperature=True))
simulation.step(10000)
