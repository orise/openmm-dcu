# 东方超算系统OpenMM使用指南

文档版本v1.0，2023年3月20日，徐顺， xushun@sccas.cn

本指南主要面向基于C86平台的**东方超算系统**用户，也可作为其他平台用户参考。C86平台的CPU处理器兼容X86指令集，类GPU处理器DCU采用兼容AMD ROCM的DTK软件开发与运行环境。

# 一、概要

OpenMM 是一个开源的分子动力学模拟软件包，主要面向CUDA 和Open CL等GPU计算环境。OpenMM为Python、C、C++甚至Fortran提供了广泛的语言绑定，可以将其用作应用程序、库或灵活的编程环境，具有极大的灵活性（通过自定义力和积分器）、开放性和高性能（特别是在最近的GPU上）。凭借其开放性和灵活的可扩展性，逐渐在其学科领域产生了显著的影响，GROMACS 在4.6~5.0 之间版本的GPU 实现，也借鉴了OpenMM 软件包。OpenMM代码在MIT和LGPL版权下开源。

- 官方网站: http://openmm.org

- 代码库: https://github.com/openmm/openmm

# 二、安装与调用

在C86平台推荐在conda环境下使用OpenMM源代码安装方式，采用DTK提供的OpenCL接口进行基于DCU的计算加速。



**配置Conda环境**

在C86平台上提供anaconda3的环境，我们在本地创建一个py3q的conda环境，并在其中安装必要的软件包，它们是接下来安装和运行OpenMM依赖的一些软件库：

```
module add apps/anaconda3/2019.10
#conda config --set auto_activate_base false
#conda deactivate
conda create -n py3q python=3 #自动在本地$HOME/.conda目录下创建
source activate py3q
#conda activate py3q
conda env list
where pip

conda install -c conda-forge scipy sympy matplotlib pandas timeseries
conda install -c conda-forge mdanalysis pymbar pdbfixer GromacsWrapper
#conda install -c conda-forge prody cctbx-base chempy ffmpeg
#conda install -c conda-forge ipykernel jupyter ipywidgets
```

以上过程需要服务器需要连接外网。在东方超算系统默认关闭HTTP外网访问，推荐通过SSH转发HTTP代理端口来连外网，参考集群[无法访问外网HTTP服务](https://gitlab.com/xushun/linux/shell/-/blob/master/cluster_faq.md#2%E6%97%A0%E6%B3%95%E8%AE%BF%E9%97%AE%E5%A4%96%E7%BD%91http%E6%9C%8D%E5%8A%A1)的FAQ文档进行解决。

通过以上执行命令创建了一个名为py3q基于Python 3的conda环境。默认创建在本地$HOME/.conda/py3q目录下。这里py3q名字可自定义，只需要和后面所涉及的操作保持名称一致即可。在conda-forge通道提供了很多Python常用库，可以直接通过conda install命令安装。对于有些conda-forge不存在的Python库，或者基于setup.py的Python源代码软件包，推荐使用pip来安装。



**使用Conda安装**

OpenMM提供conda安装的方式，在东方超算系统会出现OpenCL配置问题，不推荐这种安装方式。

```
conda install -c conda-forge openmm
```

如果出现找不到`/libstdc++.so.6:: No such file or directory`，需要在conda中安装最新gcc

```
conda install -c conda-forge gcc
```
如果是在X86+NV GPU平台，而且需要指定CUDA版本号（系统中已安装这个版本的GPU驱动程序），安装命令为
```
conda install -c conda-forge openmm cudatoolkit==10.0
```



**使用源代码安装**

使用conda直接安装OpenMM时，虽然会自动配置ROCM环境的OpenCL，但在东方超算系统上容易出现找不到OpenCL的错误（因为东方系统中DTK或ROCm有很多版本），以致于conda安装失败。推荐采用源代码安装，这也适合满足二次开发需求。以下针对C86平台，编译使用OpenCL加速的OpenMM版本。

不过在源代码安装OpenMM之前，需要加载conda环境（这里为py3q），并通过conda安装必备的依赖工具和删除OpenMM安装历史。

 ```Bash
[xushun@login10 ~]$ module add apps/anaconda3/2019.10
[xushun@login10 ~]$ source activate py3q
(py3q) [xushun@login10 ~]$ conda remove -c conda-forge gcc openmm
(py3q) [xushun@login10 ~]$ conda install -c conda-forge cmake make cython swig fftw doxygen numpy
 ```

选择安装OpenMM的GCC、MPI和DTK环境

```
[xushun@login10 ~]$ module list
Currently Loaded Modulefiles:
    1) compiler/devtoolset/7.3.1   3) compiler/dtk/22.04.2        5) mathlib/openblas-0.3.15
    2) mpi/hpcx/2.7.4/gcc-7.3.1    4) compiler/cmake/3.16.2       6) util/git/2.34.1
```

下载OpenMM源代码包并进行解压

```bash
wget https://github.com/openmm/openmm/archive/refs/tags/8.0.0.tar.gz
mv 8.0.0.tar.gz openmm-8.0.0.tar.gz
tar xzvf openmm-8.0.0.tar.gz
```
进入源代码目录
```bash
cd openmm-8.0.0
mkdir build
```

对于DTK平台提供的OpenCL环境，OpenMM软件尚不识别，运行时会出现警告信息：`WARNING: Using an unsupported OpenCL implementation`，但其实DTK/DCU和AMD ROCm/GPU兼容，推荐修改OpenMM源代码`platforms/opencl/src/OpenCLContext.cpp`文件中的isSupported函数，加入Hygon vendor识别，修改参考[常见问题](#四常见问题)2。

修改代码之后，就可以使用cmake编译，配置系统参数如

```bash
cd build
cmake -L -DCMAKE_BUILD_TYPE=Release  -DCMAKE_INSTALL_PREFIX=${HOME}/local/openmm-8.0.0 \
-DOPENMM_BUILD_CUDA_LIB=OFF -DOPENMM_BUILD_CUDA_COMPILER_PLUGIN=OFF \
-DOPENCL_INCLUDE_DIR=/public/software/compiler/dtk/dtk-22.04.2/opencl/include \
-DOPENCL_LIBRARY=/public/software/compiler/dtk/dtk-22.04.2/opencl/lib/libOpenCL.so ..
```

其中`CMAKE_INSTALL_PREFIX`选项指定安装路径，根据个人情况可修改。理论上cmake可以自动识别是否有CUDA环境，但有时会识别错误，可使用手动禁用CUDA相关编译选项

```
-DOPENMM_BUILD_CUDA_LIB=OFF -DOPENMM_BUILD_CUDA_COMPILER_PLUGIN=OFF
```

cmake的-L选项使得配置之后可以看到配置参数列表：

```cmake
BUILD_TESTING:BOOL=ON
CMAKE_BUILD_TYPE:STRING=Release
CMAKE_INSTALL_PREFIX:PATH=/public/home/xushun/local/openmm-8.0.0
CUDAToolkit_NVCC_EXECUTABLE:FILEPATH=CUDAToolkit_NVCC_EXECUTABLE-NOTFOUND
CUDAToolkit_SENTINEL_FILE:FILEPATH=CUDAToolkit_SENTINEL_FILE-NOTFOUND
CUDA_nvrtc_LIBRARY:FILEPATH=/public/home/xushun/.conda/envs/py3q/lib/libnvrtc.so
OPENMM_BUILD_AMOEBA_CUDA_LIB:BOOL=OFF
OPENMM_BUILD_AMOEBA_OPENCL_LIB:BOOL=ON
OPENMM_BUILD_AMOEBA_PLUGIN:BOOL=ON
OPENMM_BUILD_COMMON:BOOL=OFF
OPENMM_BUILD_CPU_LIB:BOOL=ON
OPENMM_BUILD_CUDA_COMPILER_PLUGIN:BOOL=OFF
OPENMM_BUILD_CUDA_LIB:BOOL=OFF
OPENMM_BUILD_C_AND_FORTRAN_WRAPPERS:BOOL=ON
OPENMM_BUILD_DRUDE_CUDA_LIB:BOOL=OFF
OPENMM_BUILD_DRUDE_OPENCL_LIB:BOOL=ON
OPENMM_BUILD_DRUDE_PLUGIN:BOOL=ON
OPENMM_BUILD_EXAMPLES:BOOL=ON
OPENMM_BUILD_OPENCL_DOUBLE_PRECISION_TESTS:BOOL=TRUE
OPENMM_BUILD_OPENCL_LIB:BOOL=ON
OPENMM_BUILD_OPENCL_TESTS:BOOL=TRUE
OPENMM_BUILD_PME_PLUGIN:BOOL=ON
OPENMM_BUILD_PYTHON_WRAPPERS:BOOL=ON
OPENMM_BUILD_RPMD_CUDA_LIB:BOOL=OFF
OPENMM_BUILD_RPMD_OPENCL_LIB:BOOL=ON
OPENMM_BUILD_RPMD_PLUGIN:BOOL=ON
OPENMM_BUILD_SHARED_LIB:BOOL=ON
OPENMM_BUILD_STATIC_LIB:BOOL=OFF
OPENMM_GENERATE_API_DOCS:BOOL=OFF
SWIG_EXECUTABLE:FILEPATH=/public/home/xushun/.conda/envs/py3q/bin/swig
SWIG_VERSION:STRING=4.0.2
```

有些默认参数也会列出来，便于确认。

开始编译，使用并行编译方式，之后可以进行安装，包括二进制程序和OpenMM Python库的安装。

```Bash
make -j6
#make test
./TestReferenceLangevinIntegrator
make install  #install CMAKE_INSTALL_PREFIX
make PythonInstall #install to conda python package site
```



**安装正确性测试**

OpenMM提供名为testInstallation的Python模块用于安装正确性测试，调用它来进行安装正确性测试。

在东方超算系统上配置sbatch脚本（sub_openmm.sh），申请单个节点4个DCU卡的资源，进行测试

```bash
#!/bin/bash
#SBATCH -J openmm
#SBATCH -p normal
#SBATCH -N 1
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --gres=dcu:4
#SBATCH -o %x-%j.log

module add apps/anaconda3/2019.10
source activate py3q
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/public/software/compiler/dtk/dtk-22.04.2/opencl/lib

date
python -m openmm.testInstallation
#python run_openmm_simulation.py
date
```

在昆山超算系统上的sbatch脚本

```bash
$ cat sub_openmm.sh
#!/bin/bash
#SBATCH -J openmm
#SBATCH -p kshdnormal
#SBATCH -N 1
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --gres=dcu:4
#SBATCH -o %x-%j.log

module rm compiler/rocm/2.9
module add compiler/rocm/dtk-22.04.2
module add apps/anaconda3/5.2.0
source activate py3q
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/public/software/compiler/rocm/dtk-22.04.2/opencl/lib

date
python -m openmm.testInstallation
#python run_openmm_simulation.py
date
```

以上脚本命名为sub_openmm.sh，使用sbatch命令提交作业

```
sbatch sub_openmm.sh
```

输出结果有如

```
$ cat openm-6643715.log
h06r3n16:4
Mon Mar 20 15:51:43 CST 2023

OpenMM Version: 8.0
Git Revision: Unknown

There are 3 Platforms available:

1 Reference - Successfully computed forces
2 CPU - Successfully computed forces
3 OpenCL - Successfully computed forces

Median difference in forces between platforms:

Reference vs. CPU: 6.31535e-06
Reference vs. OpenCL: 6.74414e-06
CPU vs. OpenCL: 7.10753e-07

All differences are within tolerance.
Mon Mar 20 15:52:15 CST 2023
```

另外，如果需要在单节点进行安装测试，可以通过salloc分配一个DCU节点，直接SSH登录该节点安装测试。

```
$ salloc -p normal --gres=dcu:4 -N 1 --exclusive
...
salloc: Waiting for resource configuration
salloc: Nodes h11r2n16 are ready for job
...
$ ssh h11r2n16 #节点名为上面salloc分配的
```



# 三、计算性能分析

在OpenMM源代码examples目录提供多个测试用例。这里选择两个实例：

- examples/benchmark.py 基准测试脚本，通过输入参数进行配置
- examples/simulatePdb.py 蛋白质水体系，使用Amber14力场和Tip3p水模型

OpenMM官方版不支持MPI并行，因此无法实现跨计算节点并行，**仅支持单节点多线程或多GPU方式**。

本实例测试在C86平台，通过OpenCL的方式调用DCU加速。

**使用CPU/DCU计算平台配置**

OpenMM不支持MPI并行，但支持单节点多线程或多GPU方式。在使用CUDA或者OpenCL加速时，支持单节点上的单卡和多卡的加速计算方式，通过OpenMM库提供的Simulation函数的platformProperties参数的DeviceIndex属性给定设备ID，用逗号分隔多个设备ID。

使用CPU单核方式

```
platform = Platform.getPlatformByName('CPU') 
simulation = Simulation(topology, system, integrator, platform)
```

使用单块DCU卡方式

```
platformProperties = {'Precision': 'double'}
platformProperties["DeviceIndex"] = "0"
platform = Platform.getPlatformByName('OpenCL')
simulation = Simulation(topology, system, integrator, platform, platformProperties)
```

使用4块DCU卡方式

```
platformProperties = {'Precision': 'double'}
platformProperties["DeviceIndex"] = "0,1,2,3"
platform = Platform.getPlatformByName('OpenCL')
simulation = Simulation(topology, system, integrator, platform, platformProperties)
```



## 1. 测试用例-基准测试脚本

OpenMM源代码提供基准测试用例文件examples/benchmark.py，可通过输入参数进行配置。新建测试脚本`slurm_bench.sh`

```bash
$ cat slurm_bench.sh
#!/bin/bash
#SBATCH -J openmm
#SBATCH -p normal
#SBATCH -N 1
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --gres=dcu:4
#SBATCH -o %x-%j.log

#env|grep -E "SLURM|ROCM|HIP"

module add apps/anaconda3/2019.10
source activate py3q
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/public/software/compiler/dtk/dtk-22.04.2/opencl/lib

date
#cd openmm/examples
python3 benchmark.py --test apoa1rf  --platform OpenCL --seconds 20
python3 benchmark.py --test apoa1rf  --platform CPU --seconds 20

python3 benchmark.py --test apoa1pme  --platform OpenCL --seconds 20
python3 benchmark.py --test apoa1pme  --platform CPU --seconds 20
date
```

**计算效率对比**

分别测试CPU和DCU的计算效率

**C86平台上**，

硬件处理器基本参数：

CPU：Hygon C86 7185 32-core Processor

DCU：gfx906:sramecc-:xnack，Hygon OpenCL

测试结果：

| 体系     | Platform       | Speed (ns/day) |
| -------- | -------------- | -------------- |
| apoa1rf  | 32 CPU threads | 8.02           |
| apoa1rf  | 1 DCU openCL   | 60.89          |
| apoa1pme | 32 CPU threads | 6.43           |
| apoa1pme | 1 DCU openCL   | 48.91          |

对比**NV GPU上**的结果

硬件处理器基本参数：

CPU：Intel(R) Xeon(R) Gold 5218 CPU @ 2.30GHz

GPU：Tesla V100S-PCIE-32GB

```
python3 benchmark.py --test apoa1pme  --platform CPU --seconds 20
python3 benchmark.py --test apoa1pme  --platform CUDA --device 1 --seconds 20
python3 benchmark.py --test apoa1pme  --platform OpenCL --device 1 --seconds 20
python3 benchmark.py --test apoa1rf  --platform CPU --seconds 20
python3 benchmark.py --test apoa1rf  --platform CUDA --device 1 --seconds 20
python3 benchmark.py --test apoa1rf  --platform OpenCL --device 1 --seconds 20
```

测试结果：

| 体系     | platform       | Speed (ns/day) |
| -------- | -------------- | -------------- |
| apoa1rf  | 64 CPU threads | 8.9            |
| apoa1rf  | 1 GPU OpenCL   | 412.50         |
| apoa1rf  | 1 GPU CUDA     | 528.68         |
| apoa1pme | 64 CPU threads | 6.80           |
| apoa1pme | 1 GPU OpenCL   | 232.55         |
| apoa1pme | 1 GPU CUDA     | 335.49         |



## 2. 测试用例-蛋白质水体系

OpenMM源代码提供蛋白质水体系用例文件examples/simulatePdb.py， 其内容如下

```python
from openmm.app import *
from openmm import *
from openmm.unit import *
from sys import stdout

pdb = PDBFile('input.pdb')
forcefield = ForceField('amber99sb.xml', 'tip3p.xml')
system = forcefield.createSystem(pdb.topology, nonbondedMethod=PME, nonbondedCutoff=1*nanometer, constraints=HBonds)
integrator = LangevinIntegrator(300*kelvin, 1/picosecond, 0.002*picoseconds)
simulation = Simulation(pdb.topology, system, integrator)
simulation.context.setPositions(pdb.positions)
simulation.minimizeEnergy()
simulation.reporters.append(PDBReporter('output.pdb', 1000))
simulation.reporters.append(StateDataReporter(stdout, 1000, step=True, potentialEnergy=True, temperature=True))
simulation.step(10000)
```
为了测试性能，我们在StateDataReporter中添加`speed=True`参数，使得Openmm的输出日志中包含"Speed (ns/day)"性能数值的估计
```
StateDataReporter(stdout, 1000, step=True, speed=True, potentialEnergy=True, temperature=True)
```

进入examples命令行提交方式为

```
$ srun -p normal -N 1 --gres=dcu:4 --exclusive python simulatePdb.py
```

在examples目录新建sbatch的提交脚本slurm_pdb.sh

```bash
$ cat slurm_pdb.sh
#!/bin/bash
#SBATCH -J openmm
#SBATCH -p normal
#SBATCH -N 1
#SBATCH --ntasks-per-node=4
#SBATCH --cpus-per-task=1
#SBATCH --exclusive
#SBATCH --gres=dcu:4
#SBATCH -o %x-%j.log

#env|grep -E "SLURM|ROCM|HIP"

module add apps/anaconda3/2019.10
source activate py3q
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/public/software/compiler/dtk/dtk-22.04.2/opencl/lib

date
#cd openmm/examples
python simulatePdb.py
date
```

**计算效率对比**

按照使用[CPU/DCU计算平台配置](#使用CPU/DCU计算平台配置)的设置修改simulatePdb.py文件，分别测试CPU和DCU的计算效率

| Platform | 最大%CPU | Speed (ns/day) |
| -------- | -------- | -------------- |
| CPU      | 1357%    | 33.6           |
| 1 DCU    | 45%      | 122            |
| 2 DCU    | 98%      | 159            |
| 3 DCU    | 160%     | 176            |
| 4 DCU    | 235%     | 193            |

对比X86 NV V100 GPU的结果

| platform | 最大%CPU | Speed (ns/day) |
| -------- | -------- | -------------- |
| CPU      | 3030%    | 44.8           |
| 1 GPU    | 100%     | 296            |
| 2 GPU    | 175%     | 366            |
| 3 GPU    | 232%     | 374            |
| 4 GPU    | 285%     | 375            |



# 四、常见问题

**1. 运行OpenMM的Python脚本时，出现undefined symbol相关的ImportError**

错误有如

```
ImportError: /public/home/xushun/.conda/envs/py3q/lib/python3.9/site-packages/openmm/_openmm.cpython-39-x86_64-linux-gnu.so: undefined symbol: _ZN6OpenMM15CustomBondForce22setGlobalParameterNameEiRKSs
```

原因是libOpenMM.so等动态库链接路径错误，正确应该链接到`CMAKE_INSTALL_PREFIX/lib`目录下相应的so文件

使用ldd和nm工具分析当前链接路径

```
ldd /public/home/xushun/.conda/envs/py3q/lib/python3.9/site-packages/openmm/_openmm.cpython-39-x86_64-linux-gnu.so
```

发现链接错误，如`$HOME/.conda/envs/py3q/lib/libOpenMM.so.8.0`，说明存在openmm安装历史，清除即可

```
conda list
pip uninstall openmm
rm -rf $HOME/.conda/envs/py3q/lib/libOpenMM*
```

再重新安装OpenMM

```
cd build
make install
make PythonInstall
```



**2. OpenMM调用OpenCL运行时出现unsupported OpenCL implementation警告**

在DTK/DCU平台运行OpenMM时，出现警告`WARNING: Using an unsupported OpenCL implementation.  Results may be incorrect.`，这是因为OpenMM不识别当前DTK的OpenCL实现。

DTK实现的OpenCL平台提供的clinfo命令可以获得Vendor名称，

```
/public/software/compiler/dtk/dtk-22.04.2/opencl/bin/clinfo
Vendor:                                        Hygon, Inc.
```

清除这条警告，需要修改一处OpenMM源代码并重新编译。

具体修改Openmm源代码`platforms/opencl/src/OpenCLContext.cpp`文件中的isSupported函数，加入Hygon vendor识别，代码修改为如下：

```c++
static bool isSupported(cl::Platform platform) {
    string vendor = platform.getInfo<CL_PLATFORM_VENDOR>();
    return (vendor.find("NVIDIA") == 0 ||
            vendor.find("Advanced Micro Devices") == 0 ||
            vendor.find("Apple") == 0 ||
            vendor.find("Hygon") == 0 ||
            vendor.find("Intel") == 0);
}
```



**3. timeseries Python库的debug**

使用pip3 安装timeseries之后，调用时出现import错误，如

```
ImportError: cannot import name 'DictType' from 'types' 
```
涉及修改的代码文件有
```
$HOME/.conda/envs/py3q/lib/python3.9/site-packages/timeseries/time_series.py"
$HOME/.conda/envs/py3q/lib/python3.9/site-packages/timeseries/utilities.py
```
将代码中的相关语句删除

```
from types import DictType
from types import IntType, LongType, DictType
```

将`if type(points) == DictType:`替换为`if isinstance(points, dict):`。

将`if type(time) == IntType or type(time) == LongType:`替换为`if isinstance(time,int):`

遇到`ImportError: cannot import name 'MutableMapping' from 'collections' `, 则还需要修改

```
$HOME/.conda/envs/py3q/lib/python3.10/site-packages/timeseries/data_frame.py
```

将`from collections import MutableMapping`替换为`from collections.abc import MutableMapping`。

相关问题链接

https://stackoverflow.com/questions/59709355/importerror-cannot-import-name-dicttype-from-types

https://stackoverflow.com/questions/70870041/cannot-import-name-mutablemapping-from-collections




## 参考链接

- OpenMM常见问题 https://github.com/openmm/openmm/wiki/Frequently-Asked-Questions#nan
- Slurm资源管理与作业调度系统安装配置 http://hmli.ustc.edu.cn/doc/linux/slurm-install/index.html
- SLURM 使用参考 http://faculty.bicmr.pku.edu.cn/~wenzw/pages/slurm.html
- PDBFixer https://github.com/openmm/pdbfixer
